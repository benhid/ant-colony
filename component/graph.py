import math


class Grid:
    def __init__(self, nodes: list, distance=None):
        self.nodes = nodes
        self.distance = distance if distance is not None else self.euclidean
        self.edges = self.create_edges()

    def create_edges(self):
        edges = {}

        for node_a in self.nodes:
            for node_b in self.nodes:
                if node_a[0] != node_b[0] or node_a[1] != node_b[1]:
                    edges[node_a, node_b] = Edge(node_a, node_b, length=self.distance(node_a, node_b))

        return edges

    def get_edge(self, node_a, node_b):
        return self.edges[node_a, node_b]

    def get_edges(self) -> dict:
        return self.edges

    def get_nodes(self) -> list:
        return self.nodes

    def number_of_nodes(self) -> int:
        return len(self.nodes)

    def set_pheromones(self, level: float = .01):
        for edge in self.edges.values():
            edge.update_pheromone(level)

    @staticmethod
    def euclidean(node_a: tuple, node_b: tuple) -> float:
        dist = [(a - b) ** 2 for a, b in zip(node_a, node_b)]
        return math.sqrt(sum(dist))


class Edge:
    def __init__(self, node_a, node_b, length: float, pheromone: float = .1):
        self.node_a = node_a
        self.node_b = node_b
        self.length = length
        self.pheromone = pheromone

    def update_pheromone(self, level: float):
        self.pheromone = level

    def get_pheromone(self) -> float:
        return self.pheromone

    def get_length(self) -> float:
        return self.length