import bisect
import itertools
import random

from aco.component.graph import Grid


class Ant:
    def __init__(self, alpha=1, beta=3) -> None:
        self.alpha: int = alpha
        self.beta: int = beta
        self.grid: Grid = None
        self.distance: int = 0
        self.visited: list = []
        self.unvisited: list = []
        self.traveled: list = []

    def init(self, grid: Grid, start=None):
        self.grid = grid
        if start is None:
            self.start = self.grid.get_nodes()[random.randrange(len(self.grid.get_nodes()))]
        else:
            self.start=start
        self.visited = [self.start]
        self.unvisited = [node for node in self.grid.get_nodes() if node is not self.start]
        self.traveled = []
        return self

    def can_move(self):
        return len(self.traveled) != len(self.visited)

    def move(self):
        choice = self.choose_move()
        return self.make_move(choice)

    def choose_move(self):
        if len(self.unvisited) == 0:
            return None
        if len(self.unvisited) == 1:
            return self.unvisited[0]

        try:
            node = self.visited[-1]
        except IndexError:
            node = None

        probabilities = []
        for potential_node in self.unvisited:
            edge = self.grid.get_edge(node, potential_node)
            probabilities.append(self._get_probability(edge))

        cumdist = list(itertools.accumulate(probabilities)) + [sum(probabilities)]
        return self.unvisited[bisect.bisect(cumdist, random.random() * sum(probabilities))]

    def make_move(self, dest):
        ori = self.visited[-1]

        if dest is None:
            if self.can_move() is False:
                return None
            else:
                dest = self.start
        else:
            self.visited.append(dest)
            self.unvisited.remove(dest)

        edge = self.grid.get_edges()[ori, dest]
        self.traveled.append(edge)
        self.distance += edge.get_length()
        return edge

    def _get_probability(self, edge):
        # Ant makes a decision on what city to go to using this probability:
        return edge.get_pheromone() ** self.alpha \
               * 1/(edge.get_length()) ** self.beta

    def get_path(self):
        return self.traveled

    def get_distance(self):
        return self.distance
