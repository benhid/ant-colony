from copy import copy
import random

from aco.component.ant import Ant
from aco.component.graph import Grid
from aco.util.observer import Observable, DefaultObservable


class ACO:
    def __init__(self, grid: Grid, alpha: int, beta: int, rho: float,
                 t0: float, max_evaluations: int,
                 population_size: int, observable: Observable):
        self.grid = grid
        self.alpha = alpha
        self.beta = beta
        self.rho = rho # pheromone decay coefficient (evaporation rate)
        self.t0 = t0 # initial value of the pheromones
        self.max_evaluations = max_evaluations
        self.population_size = population_size
        self.evaluations: int = 0
        self.colony: list = []
        self.global_best: Ant = None
        self.observable: Observable = observable


class AntColony(ACO):
    def __init__(self, grid: Grid, alpha: int = 1, beta: int = 3, rho: float = .8,
                 t0: float = .01, max_evaluations: int = 100, population_size: int = 10,
                 observable: Observable = DefaultObservable()):
        super(AntColony, self).__init__(
            grid,
            alpha,
            beta,
            rho,
            t0,
            max_evaluations,
            population_size,
            observable)

    def run(self) -> None:
        self.grid.set_pheromones(self.t0)
        self.colony = self._create_colony()

        while not self._is_stopping_condition_reached():
            self._reset_colony()
            self._move_colony()
            self._update_trails()
            local_best = sorted(self.colony, key=lambda ant: ant.get_distance())[0]

            if self.global_best is None or local_best.get_distance() < self.global_best.get_distance():
                self.global_best = copy(local_best)

            self._update_progress()

    def _is_stopping_condition_reached(self) -> bool:
        return self.evaluations >= self.max_evaluations

    def _reset_colony(self):
        for ant in self.colony:
            ant.init(ant.grid)

    def _create_colony(self) -> list:
        ants = []

        for _ in range(self.population_size):
            start = random.randrange(self.grid.number_of_nodes())
            ants.append(
                Ant(self.alpha, self.beta).init(grid=self.grid, start=self.grid.get_nodes()[start])
            )

        return ants

    def _move_colony(self):
        ants_done = 0

        while ants_done < len(self.colony):
            ants_done = 0
            for ant in self.colony:
                if ant.can_move():
                    edge = ant.move()
                    edge.pheromone = max(self.t0, edge.get_pheromone() * self.rho)
                else:
                    ants_done += 1

    def _update_trails(self):
        # Pheromone values are updated by all the ants that have completed the tour
        for ant in self.colony:
            for edge in ant.get_path():
                edge.pheromone = \
                    max(self.t0, (1-self.rho)*edge.get_pheromone() + (1/ant.get_distance()))

    def _update_progress(self):
        self.evaluations += 1

        observable_data = {'evaluation': self.evaluations}
        self.observable.notify_all(**observable_data)

    def get_result(self) -> Ant:
        return self.global_best
