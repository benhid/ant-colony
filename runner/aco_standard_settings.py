from matplotlib import pyplot as plt

from aco.algorithm.aco import AntColony
from aco.component.graph import Grid
from aco.util.observer import BasicObserver
from aco.runner.test import test_files


def main() -> None:
    algorithm = AntColony(grid=Grid(nodes=test_files.att48()))
    #algorithm.observable.register(observer=BasicObserver())
    algorithm.run()

    # Results
    best_ant = algorithm.get_result()
    print("Minimum length:", best_ant.get_distance())

    # Plot
    fig = plt.figure(figsize=(10, 6))
    ax = fig.add_subplot(111)
    for edge in best_ant.get_path():
        ax.plot([edge.node_a[0], edge.node_b[0]], [edge.node_a[1], edge.node_b[1]], '--', marker='o')
    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    main()
