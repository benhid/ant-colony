def square() -> list:
    return [(0, 0), (1, 0), (0, 1), (1, 1)]


def fig() -> list:
    return [(0, 1), (1, 0), (6, 1), (2, 1), (12, 22), (9, 21)]


def xqf131() -> list:
    return _read_test_files("./test/xqf131.tsp")


def att48() -> list:
    return _read_test_files("./test/att48.tsp")


def _read_test_files(file: str) -> list:
    with open(file, "r") as tsp:
        test_coords = []
        for line in tsp:
            _, x, y = line.split()
            test_coords.append((int(x), int(y)))
    return test_coords
