from abc import ABCMeta, abstractmethod
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Observable(object):
    def register(self, observer):
        pass

    def deregister(self, observer):
        pass

    def notify_all(self, *args, **kwargs):
        pass


class DefaultObservable(Observable):
    def __init__(self):
        self.observers = []

    def register(self, observer):
        if observer not in self.observers:
            self.observers.append(observer)

    def deregister(self, observer):
        if observer in self.observers:
            self.observers.remove(observer)

    def notify_all(self, *args, **kwargs):
        for observer in self.observers:
            observer.update(*args, **kwargs)


class BasicObserver(Observable):
    def __init__(self, frequency: float = 1.0) -> None:
        self.display_frequency = frequency

    def update(self, *args, **kwargs):
        evaluations = kwargs['evaluation']
        if (evaluations % self.display_frequency) == 0:
            logger.info("Evaluation = " + str(evaluations))